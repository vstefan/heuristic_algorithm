# Heuristic Implementation 1
This project seeks to compare various iterative improvement algorithms on instances of the permutation flow-shop scheduling problems 

## Project Structure
* **src** - folder containing the source code
* **instances** - folder with the text files containing all instances used during this project.
* **results** - folder where all the results from the experiments are written as text files
* **s_test** - folder containing all the data recovered from tests during the experiment as .csv files and the statistical tests implemented in a Jupyter Notebook.

## Getting Started 
Use the make command to compile the project. It can then be launched with the following command: 
```
./flowshopWCT <pivoting rule> <initial solution> <neighborhood> <instance>
```
* <instance type> is either 50 or 100 to designate either group;
* <initial solution> is rand or rz to respectively identify a random permutation or the simple RZ heuristic
* <neighborhood> can be t, e, i if a simple neighborhood is used or tei, tie to designate a series of ordered neighborhoods
* <pivoting rule> is f, b or vnd to designate first-improvement, best-improvement or VND
**/!\ VND will not work if it is given a neighborhood other than tie or tei.  Similarly first/best-improvement only work with the t, e or i neighborhoods.**
Here is an example of correct use:
```
./flowshopWCT b rz e 50
```

Use the make clean command to remove all .o files and the executable.