CFILES = ./src/flowshop.cpp ./src/pfspinstance.cpp ./src/initsol.cpp ./src/neighborhood.cpp ./src/pivot.cpp ./src/timer.cpp ./src/solve.cpp ./src/container.cpp
OFILES = $(CFILES:.cpp=.o)
CXXFLAGS = -O3 -Wall -Werror -std=c++17
CC = g++
LDFLAGS=-lpthread

PROG = flowshopWCT

all: $(PROG)

$(PROG): $(OFILES)
	$(CC) $(CFLAGS) -o $(PROG) $(OFILES) $(LDFLAGS)


clean: 
	/bin/rm -rf $(OFILES) $(PROG)
