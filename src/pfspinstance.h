/***************************************************************************
 *   Copyright (C) 2012 by Jérémie Dubois-Lacoste   *
 *   jeremie.dl@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef _PFSPINSTANCEWT_H_
#define _PFSPINSTANCEWT_H_

#include <string>
#include <vector>
#include <tuple>

using namespace std;

class PfspInstance{
  private:
    int nbJob;
    int nbMac;
    std::vector< long int > dueDates;
    std::vector< long int > priority;

    std::vector< std::vector <long int> > processingTimesMatrix;

    //Matrix containing all the steps needed to compute the WCT of the last processed candidate solution
    std::vector< std::vector <long int> > matrixWCT;

  public:
    PfspInstance();
    ~PfspInstance();

    /* Read write privates attributs : */
    int getNbJob();
    int getNbMac();

    /* Allow the memory for the processing times matrix : */
    void allowMatrixMemory(int nbJ, int nbM);

    /* Read\Write values in the matrix : */
    long int getTime(int job, int machine);
    void setTime(int job, int machine, long int processTime);

    long int getDueDate(int job);
    void setDueDate(int job, int value);

    long int getPriority(int job);
    void setPriority(int job, int value);

    /* Read Data from a file : */
    //bool readDataFromFile(char * fileName);

    /* Read Data from a file : */
    bool readDataFromFile(const string& fileName);

    long int getTotalTime();

    /**
    * Fill a vector with tuples containing <weighted sum of processing times of job j, j>.
    * The tuples are sorted in ascending order of processing time.
    * 
    * @param weightedTime Output vector
    */
    void weightedProcessingTime(vector<tuple<float, int> >& weightedTime);

    long int computeWCT (const vector< int > & sol);

    /**
    * Compute the WCT for a solution that only contains a partial number of jobs
    * @param sol Input vector with the candidate solution
    * @param partialJobNb Number of jobs in the current solution
    * @return score of the current candidate solution
    */
    long int computePartialWCT(const vector< int >& sol, int partialJobNb);

    /**
    * Compute the WCT for a solution and store the whole process in a matrix
    * @param sol Input vector with the candidate solution
    * @param start Index of the job from which the computation of the WCT must be started
    * @return score of the current candidate solution
    */
    long int computLastingWCT(const vector< int >& sol, int start);

};

#endif
