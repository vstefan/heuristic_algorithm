#include "neighborhood.h"
#include <vector>
#include <iostream>

using namespace std;
void ExchangeNeighborhood::getNeighbors(const vector<int>& sol, vector<vector<int> >& n)
{
	//for each element in the solution (except the last one) swap that element with all subsequent elements
	for (unsigned i = 1; i < sol.size() - 1; i++)
	{
		for (unsigned j = i + 1; j < sol.size(); j++)
		{
			vector<int> neighbor = sol;
			iter_swap(neighbor.begin()+ i,neighbor.begin()+j);
			n.push_back(neighbor);
		}
	}
}

ExchangeNeighborhood::~ExchangeNeighborhood()
{
}

void TransposeNeighborhood::getNeighbors(const vector<int>& sol, vector<vector<int> >& n)
{
	for (unsigned i = 1; i < sol.size() - 1; i++) //for each element in the solution (except the last one) swap the element with the element that follows it
	{
		
		vector<int> neighbor = sol;
		iter_swap(neighbor.begin() + i, neighbor.begin() + i + 1);
		n.push_back(neighbor);
		
	}
	//swap the first and last elements
	vector<int> neighbor = sol;
	iter_swap(neighbor.begin() + 1, neighbor.end()-1);
	n.push_back(neighbor);
}

TransposeNeighborhood::~TransposeNeighborhood()
{
}

void InsertNeighborhood::getNeighbors(const vector<int>& sol, vector<vector<int> >& n)
{
	//for each element in the solution, insert it in any other place of the vector
	for (unsigned i = 1; i < sol.size(); i++)
	{
		for (unsigned j = 1; j < i - 1; j++)
		{
			vector<int> neighbor = sol;
			move_element(i, j, neighbor);
			n.push_back(neighbor);
		}
		
		//we start at i+2 because inserts in a vector are done before the specified position
		//thus vector.insert(i + 1, vector.at(i)) will be the same as a vector with no insertion
		for (unsigned j = i+2; j < sol.size() + 1; j++) 
		{
			vector<int> neighbor = sol;
			move_element(i, j, neighbor);
			n.push_back(neighbor);
		}
	}
	
}

InsertNeighborhood::~InsertNeighborhood()
{
}

void move_element(size_t start, size_t dst, std::vector<int>& v)
{

	const size_t final_dst = dst > start ? dst - 1 : dst;
	int tmp = *(v.begin() + start);

	v.erase(v.begin() + start);
	v.insert(v.begin() + final_dst, tmp);
}

Neighborhood::~Neighborhood()
{
}
