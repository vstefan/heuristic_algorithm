/***************************************************************************
 *   Copyright (C) 2012 by Jérémie Dubois-Lacoste   *
 *   jeremie.dl@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <thread>
#include <filesystem>
#include <random>

#include "pfspinstance.h"
#include"neighborhood.h"
#include "pivot.h"
#include "initsol.h"
#include "timer.h"
#include "solve.h"
#include "container.h"
#include "semaphore.h"


using namespace std;

/* Fill the solution with numbers between 1 and nbJobs, shuffled */
void randomPermutation(int nbJobs, vector< int > & sol)
{
  vector<bool> alreadyTaken(nbJobs+1, false); // nbJobs elements with value false
  vector<int > choosenNumber(nbJobs+1, 0);

  int nbj = 0;
  for (int i = nbJobs; i >= 1; --i)
  {
    int rnd = generateRndPosition(1, i);
    int nbFalse = 0;

    
    /* find the rndth cell with value = false : */
    int j;
    for (j = 1; nbFalse < rnd; ++j)
      if ( ! alreadyTaken[j] )
        ++nbFalse;
    --j;

    sol[j] = i;

    ++nbj;
    choosenNumber[nbj] = j;

    alreadyTaken[j] = true;
  }
}


int main() {
    constexpr unsigned num_runs = 5;
    constexpr unsigned num_threads = 64;
    const string input_dir = "./instances/";
    const string outname_base = "./results/result_";
    const string outname_base_run_time = "./results/run_time/result_";
    Semaphore s(num_threads);
    mutex ofile_lock_ils, ofile_lock_sa;

    //initialize rand number seed
    //random_device rd();
    //mt19937 gen(rd); // rd seed for true random seed
    mt19937 gen(123);  // static seed for reproductability
    uniform_int_distribution<> seed_distrib;
    
    //apply both sls methods on all instances 5 times
    
    for(const auto& file: filesystem::directory_iterator(input_dir)) {
        const string fpath = file.path().string();

        for(unsigned run = 0; run < num_runs; run++) {
            //launch ILS
	        uint32_t seed = seed_distrib(gen);
	        
            s.acquire();
            thread t_ils( [fpath, run, outname_base, &ofile_lock_ils, seed, &s ] { 
                PfspInstance instance;

                if(instance.readDataFromFile(fpath))  {
                    vector<int> solution(instance.getNbJob()+1);
                    simpleRZ(solution, instance);
                    long int score = ils(instance, solution, seed);

                    //write output
                    const string fname = outname_base + "ils_" + to_string(instance.getNbJob()) + ".csv";
                    scoped_lock sl(ofile_lock_ils);
                    ofstream ofile(fname,std::ios_base::app);
                    ofile << fpath << ", " << run << ", " << score << endl;
                }

                s.release();
            });
            t_ils.detach();
            
            
            //launch SA
	        seed = seed_distrib(gen);
	    
            s.acquire();
            thread t_sa( [fpath, run, outname_base, &ofile_lock_sa, seed, &s ] { 
                PfspInstance instance;

                if(instance.readDataFromFile(fpath))  {
                    vector<int> solution(instance.getNbJob()+1);
                    simpleRZ(solution, instance);
                    long int first_score = instance.computeWCT(solution);
                    long int score = sa(instance, solution, seed);

                    //write output
                    const string fname = outname_base + "sa_" + to_string(instance.getNbJob()) + ".csv";
                    scoped_lock sl(ofile_lock_sa);
                    ofstream ofile(fname,std::ios_base::app);
                    ofile << fpath << ", " << run <<  ", " << score << endl;
                }

                s.release();

            });
            t_sa.detach();

        }
    }

    //Measure the run-time distribution of both methods on the first 2 instances
    for (unsigned i = 1; i <= 2; i++)
    {
        const string fpath = "./instances/50_20_0" + to_string(i);
        for (unsigned runs = 0; runs < 25; runs++)
        {

	        uint32_t seed = seed_distrib(gen);
	    
            s.acquire();
            thread t_ils_run([fpath, runs, outname_base_run_time, seed, &s, i] {
                PfspInstance instance;
                vector<Container> c;
                if (instance.readDataFromFile(fpath))
                {
                    vector<int> solution(instance.getNbJob() + 1);
                    simpleRZ(solution, instance);
                    ils_run_time(instance, solution, c, seed);

                    //write output
                    const string fname = outname_base_run_time + "ils_" + to_string(i)+ "_" + to_string(runs) + ".csv";
                    ofstream ofile(fname);
                    ofile << "computation time, solution quality\n";
                    for (Container ctr:c)
                    {
                        ofile << to_string(ctr.getTime()) << ", " << to_string(ctr.getQuality()) << endl;
                    }
                }
                s.release();
            });
            t_ils_run.detach();

            
	        seed = seed_distrib(gen);
	    
            s.acquire();
            thread t_sa_run([fpath, runs, outname_base_run_time, seed, &s, i] {
                PfspInstance instance;
                vector<Container> c;
                if (instance.readDataFromFile(fpath))
                {
                    vector<int> solution(instance.getNbJob() + 1);
                    simpleRZ(solution, instance);
                    sa_run_time(instance, solution, c, seed);

                    //write output
                    const string fname = outname_base_run_time + "sa_" + to_string(i) + "_" + to_string(runs) + ".csv";
                    ofstream ofile(fname);
                    ofile << "computation time, solution quality\n";
                    for (Container ctr : c)
                    {
                        ofile << to_string(ctr.getTime()) << ", " << to_string(ctr.getQuality()) << endl;
                    }
                }
                s.release();
                });
            t_sa_run.detach();
            
        }
        
    }
    //wait for all threads to finish
    for (unsigned i = 0; i < num_threads; i++)
        s.acquire();

    
    
    return 0;
}


/***********************************************************************/

/*

int main(int argc, char *argv[])
{

    const string fname = "instances/50_20_01";
    std::vector<int> solution;
    PfspInstance instance;
    string name;

    if (instance.readDataFromFile(fname)) {
        solution.resize(instance.getNbJob() + 1);

        
    }
    */
    /*
    

    const string fname = "instances/50_20_01";

    for(unsigned i = 0; i < 100; i++) {

        s.acquire();



        thread t([fname, &s] {
            std::vector<int> solution;
            PfspInstance instance;
            string name;

            if (instance.readDataFromFile(fname)) { 
                solution.resize(instance.getNbJob() + 1);

                simpleRZ(solution, instance);

                long int score = ils(instance, solution);

                cout << "score " << score << "\n";
            }

            s.release();
        });


        t.detach();
    }

    
    */
    /*
    if (argc != 5)
    {
        cout << "Incorrect use of the program " << argv[0] << "\n";
        cout << "the correct use is : " << argv[0] << " <pivoting rule> <initial solution> <neighborhood> <instance>\n";
        return 1;
    }
    //Read the user input
    string pivot = argv[1];
    string init = argv[2];
    string neighbor = argv[3];
    string inst = argv[4];

    long int (*pivot_rule_ii)(PfspInstance&, Neighborhood*, std::vector<int>&) = nullptr;
    void (*init_sol)(std::vector<int>&, PfspInstance&);
    int n;

    Neighborhood* n1[3];
    Neighborhood* n2[3];

    n1[0] = new TransposeNeighborhood();
    n1[1] = new ExchangeNeighborhood();
    n1[2] = new InsertNeighborhood();

    n2[0] = new TransposeNeighborhood();
    n2[1] = new InsertNeighborhood();
    n2[2] = new ExchangeNeighborhood();

    // initialize random seed:
    srand(123456);

    string name = inst + "_20";
    string instType = "instances/" + name + "_";
    string fileName = "results/" + name + "_" + init + "_" + neighbor + "_" + pivot + ".txt";
    //solve the instance
    ofstream fout1;
    fout1.open(fileName);
    fout1 << "Instance type = " << name << "\n";
    fout1 << "Initial solution " << init << "\n";
    fout1 << "Neighborhood = " << neighbor << "\n";
    fout1 << "Pivoting rule = " << pivot << "\n\n";

    if (pivot == "f"){
        pivot_rule_ii = &first_imp_WCT_matrix;
    } 
    else if(pivot == "b"){
        pivot_rule_ii = &best_imp_WCT_matrix;
    }
    else if(pivot == "vnd")
    {
        //The correct pivoting rule is already included in the solve function
    }
    else
    {
        cout << "incorrect pivot argument\n";
        //close file and free memory
        fout1.close();
        for (int i = 0; i < 3; i++)
        {
            delete(n1[i]);
            delete(n2[i]);
        }
        return 1;
    }

    if (init == "rand") { 
        init_sol = &randomPermutation; 
    }
    else if (init == "rz") { init_sol = &simpleRZ; }
    else
    {
        cout << "incorrect initial solution argument\n";
        //close file and free memory
        fout1.close();
        for (int i = 0; i < 3; i++)
        {
            delete(n1[i]);
            delete(n2[i]);
        }
        return 1;
    }

    if (neighbor == "t") {
        n = 0; 
        solve(init_sol, n1[n], pivot_rule_ii, fout1, instType);
    }
    else if (neighbor == "e") {
        n = 1; 
        solve(init_sol, n1[n], pivot_rule_ii, fout1, instType);
    }
    else if (neighbor == "i") { 
        n = 2;
        solve(init_sol, n1[n], pivot_rule_ii, fout1, instType);
    }
    else if (neighbor == "tei") {
        solve(init_sol, n1, fout1, instType);
    }
    else if (neighbor == "tie") {
        solve(init_sol, n2, fout1, instType);
    }
    else
    {
        cout << "incorrect neighborhood argument\n";
        //close file and free memory
        fout1.close();
        for (int i = 0; i < 3; i++)
        {
            delete(n1[i]);
            delete(n2[i]);
        }
        return 1;
    }

    //close file and free memory
    fout1.close();
    for (int i = 0; i < 3; i++)
    {
        delete(n1[i]);
        delete(n2[i]);
    }

    */
   /*
  return 0;
  
}

*/
