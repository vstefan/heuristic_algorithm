#ifndef _TIMER_H_
#define _TIMER_H_
#include<chrono>
#include <vector>

class Timer
{
public:
	/**
	* Start a timer
	* @param durations Input vector that will contain the execution time of the timed code
	*/
	explicit Timer(std::vector<long long>* durations);
	~Timer();

	/**
	* Stop the timer
	*/
	void stop();

private:
	std::vector<long long>* v; //vector of execution times    
	std::chrono::time_point<std::chrono::high_resolution_clock> startPoint;
};
#endif
