#ifndef _NEIGHBORHOOD_
#define _NEIGHBORHOOD_

#include <vector>

using namespace std;

class Neighborhood
{
public:
	/**
	* Compute all neighbors of the current position
	* @param sol Current job ordering
	* @param n Output vector that will contain all neighbors
	*/
	virtual void getNeighbors(const vector<int>& sol, vector<vector<int> >& n) =0;
	virtual ~Neighborhood();

private:

};

class ExchangeNeighborhood: public Neighborhood
{
public:
	/**
	* Returns all the neighbors of a candidate solution.
	* The neighbors are obtained by exchanging 2 elements of the initial solution
	*
	* @param sol is a vector of integers representing an initial solution
	* @param n Output vector containing all the neighbors of the initial candidate solution
	*/
	void getNeighbors(const vector<int>& sol, vector<vector<int> >& n) override;
	~ExchangeNeighborhood();
private:

};

class TransposeNeighborhood : public Neighborhood
{
public:
	/**
	* Returns all the neighbors of a candidate solution.
	* The neighbors are obtained by exchanging 2 adjacent elements of the initial solution
	* 
	* @param sol is a vector of integers representing an initial solution
	* @param n Output vector containing all the neighbors of the initial candidate solution
	*/
	void getNeighbors(const vector<int>& sol, vector<vector<int> >& n) override;
	~TransposeNeighborhood();
private:

};

class InsertNeighborhood : public Neighborhood
{
public:

	/**
	* Returns all the neighbors of a candidate solution.
	* The neighbors are obtained by insertin 1 elements of the initial solution in another postion
	*
	* @param sol is a vector of integers representing an initial solution
	* @param n Output vector containing all the neighbors of the initial candidate solution
	*/
	void getNeighbors(const vector<int>& sol, vector<vector<int> >& n) override;
	~InsertNeighborhood();
private:

};

/**
* Insert an element of a vector in another position
* 
* @param start Index of the element that will be moved
* @param dst Index before which the element will be inserted
* @param v Input vector
*/
void move_element(size_t start, size_t dst, std::vector<int>& v);

#endif
