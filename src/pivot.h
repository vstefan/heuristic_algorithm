#ifndef _PIVOT_
#define _PIVOT_

#include "neighborhood.h"
#include "pfspinstance.h"
#include "container.h"

#include <random>


/**
* Apply the best improvement pivoting rule on a pfsp instance
* @param pfsp Input intsance of the pfsp
* @param n Neighborhood relation to be used
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @return best score found during the search
*/
long int best_imp(PfspInstance& pfsp, Neighborhood* n, vector<int>& sol);

/**
* Apply the best improvement pivoting rule on a pfsp instance.
* Avoids recomputing the score of each solution candidate by keeping track of the last computed WCT
* @param pfsp Input intsance of the pfsp
* @param n Neighborhood relation to be used
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @return best score found during the search
*/
long int best_imp_WCT_matrix(PfspInstance& pfsp, Neighborhood* n, vector<int>& sol);

/**
* Apply the first improvement pivoting rule on a pfsp instance
* @param pfsp Input intsance of the pfsp
* @param n Neighborhood relation to be used
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @return best score found during the search
*/
long int first_imp(PfspInstance& pfsp, Neighborhood* n, vector<int>& sol);

/**
* Apply the first improvement pivoting rule on a pfsp instance.
* Avoids recomputing the score of each solution candidate by keeping track of the last computed WCT
* @param pfsp Input intsance of the pfsp
* @param n Neighborhood relation to be used
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @return best score found during the search
*/
long int first_imp_WCT_matrix(PfspInstance& pfsp, Neighborhood* n, vector<int>& sol);

/**
* Apply the VND first improvement pivoting rule on a pfsp instance.
* Avoids recomputing the score of each solution candidate by keeping track of the last computed WCT
* @param pfsp Input intsance of the pfsp
* @param n Array(of size 3) of neighborhood relations to be used
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @return best score found during the search
*/
long int vnd(PfspInstance& pfsp, Neighborhood** n, vector<int>& sol);

/**
* Finds the index of the first different element between 2 vectors
* @param v1 Input vector
* @param v2 Input vector
* @return Index of the first difference
*/
int firstDiff(vector<int>& v1, vector<int>& v2);

/**
* Apply an iterated local search to an instance of the pfsp
* @param pfsp Input intsance of the pfsp
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @param seed Random seed
* @return best score found during the search
*/
long int ils(PfspInstance& pfsp, vector<int>& sol, uint32_t seed);

/**
* perturb an inital solution by changing the position of some elements randomly
* @param sol Input/output vector that initially contains the starting solution and will contain the perturbed solution
* @param gamma number of elements to be randomly inserted at different positions
* @param gen Random number generator
*/
void perturbation(vector<int>& sol, const int gamma, std::mt19937& gen);

/**
* Apply simulated annealing to an instance of the pfsp
* @param pfsp Input intsance of the pfsp
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @param seed Random seed
* @return best score found during the search
*/
long int sa(PfspInstance& pfsp, vector<int>& sol, uint32_t seed);


/**
* Apply an iterated local search to an instance of the pfsp and keep track of all improvements
* @param pfsp Input intsance of the pfsp
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @param dist Vector containing all improvements made to the intial solution and duration at which they are found
* @param seed Random seed
*/
void ils_run_time(PfspInstance& pfsp, vector<int>& sol, vector<Container>& dist, uint32_t seed);

/**
* Apply simulated annealing to an instance of the pfsp and keep track of all improvements
* @param pfsp Input intsance of the pfsp
* @param sol Input/output vector that initially contains the starting solution and will contain the best found solution
* @param dist Vector containing all improvements made to the intial solution and duration at which they are found
* @param seed Random seed
*/
void sa_run_time(PfspInstance& pfsp, vector<int>& sol, vector<Container>& dist, uint32_t seed);

#endif 
