#include <iostream>
#include <fstream>
#include <thread>
#include <filesystem>

#include "pfspinstance.h"
#include"neighborhood.h"
#include "pivot.h"
#include "initsol.h"
#include "timer.h"
#include "solve.h"
#include "container.h"
#include "semaphore.h"

using namespace std;

/* Fill the solution with numbers between 1 and nbJobs, shuffled */
void randomPermutation(int nbJobs, vector< int > & sol)
{
  vector<bool> alreadyTaken(nbJobs+1, false); // nbJobs elements with value false
  vector<int > choosenNumber(nbJobs+1, 0);

  int nbj = 0;
  for (int i = nbJobs; i >= 1; --i)
  {
    int rnd = generateRndPosition(1, i);
    int nbFalse = 0;

    
    /* find the rndth cell with value = false : */
    int j;
    for (j = 1; nbFalse < rnd; ++j)
      if ( ! alreadyTaken[j] )
        ++nbFalse;
    --j;

    sol[j] = i;

    ++nbj;
    choosenNumber[nbj] = j;

    alreadyTaken[j] = true;
  }
}


int main() {
    srand(1234567);
    

    const string input_dir = "./instances/";
    const string outname_base = "./results/result_";
    const string outname_base_run_time = "./results/run_time/result_";

    const string fpath = "./instances/50_20_01";
    
    PfspInstance instance;
    vector<Container> c;
    if (instance.readDataFromFile(fpath))
    {
	vector<int> solution(instance.getNbJob() + 1);
	simpleRZ(solution, instance);
	sa_run_time(instance, solution, c, 123);
	
	//write output
	const string fname = outname_base_run_time + "sa_" + to_string(instance.getNbJob()) + "_1.txt";
	ofstream ofile(fname);
	ofile << "computation time, solution quality\n";
	for (Container ctr : c)
	{
	    ofile << to_string(ctr.getTime()) << ", " << to_string(ctr.getQuality()) << endl;
	}
    }

    return 0;
}

