#ifndef _SOLVE_H_
#define _SOLVE_H_

#include "pfspinstance.h"
#include <vector>
#include <fstream>
#include "neighborhood.h"

/** 
* Solve all instances of a certain type with a given type with a certain pivoting rule, neighborhood and starting solution
* Write all results in a file
* 
* @param init_sol Pointer to a function computing the initial solution
* @param n Neighborhood relation to be used
* @param pivot Pointer to a function applyin the pivoting rule
* @param fout1 Output file where all results are written
* @param fileName Instance type of the pfsp (50 jobs or 100 jobs)
*/
int solve(void(*init_sol)(std::vector<int>&, PfspInstance&), Neighborhood* n,
	long int(*pivot)(PfspInstance&, Neighborhood*, std::vector<int>&), ofstream& fout1, string fileName);


/**
* Solve all instances of a certain type with a given type, neighborhood, starting solution and with a VND first-improvement pivoting rule
* Write all results in a file
*
* @param init_sol Pointer to a function computing the initial solution
* @param n Array of neighborhood relations to be used
* @param fout1 Output file where all results are written
* @param fileName Instance type of the pfsp (50 jobs or 100 jobs)
*/
int solve(void(*init_sol)(std::vector<int>&, PfspInstance&), Neighborhood** n, ofstream& fout1, string fileName);

#endif