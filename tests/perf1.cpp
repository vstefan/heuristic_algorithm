//g++ -std=c++20 -Wall perf1.cpp -lbenchmark -Ofast

#include <random>
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

#include <benchmark/benchmark.h>

static void BM_mt_uniform_int(benchmark::State& state) {
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distrib(1,100000);
    
    // Perform setup here	
    for (auto _ : state) {
	auto val = distrib(gen);
	benchmark::DoNotOptimize(val);
    }
}


static void BM_mt_uniform_real(benchmark::State& state) {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> distrib(1.0,2.0);
    
    // Perform setup here
    for (auto _ : state) {
	auto val = distrib(gen);
	benchmark::DoNotOptimize(val);
    }
}



static void BM_rd_mt_uniform_int(benchmark::State& state) {
    // Perform setup here	
    for (auto _ : state) {
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> distrib(1,100000);
	
	auto val = distrib(gen);
	benchmark::DoNotOptimize(val);
    }
}


static void BM_rd_mt_uniform_real(benchmark::State& state) {
    // Perform setup here
    for (auto _ : state) {
	random_device rd;
	mt19937 gen(rd());
	uniform_real_distribution<> distrib(1.0,2.0);
	
	auto val = distrib(gen);
	benchmark::DoNotOptimize(val);
    }
}

static void BM_ss_mt_uniform_int(benchmark::State& state) {
    // Perform setup here	
    for (auto _ : state) {
	mt19937 gen(123);
	uniform_int_distribution<> distrib(1,100000);
	
	auto val = distrib(gen);
	benchmark::DoNotOptimize(val);
    }
}


static void BM_ss_mt_uniform_real(benchmark::State& state) {
    // Perform setup here
    for (auto _ : state) {
	mt19937 gen(123);
	uniform_real_distribution<> distrib(1.0,2.0);
	
	auto val = distrib(gen);
	benchmark::DoNotOptimize(val);
    }
}

static void BM_rand_int(benchmark::State& state) {
    // Perform setup here
    srand(time(nullptr));
    for (auto _ : state) {
	int irand = rand();
	benchmark::DoNotOptimize(irand);
    }
}


static void BM_srand_rand_int(benchmark::State& state) {
    // Perform setup here
    for (auto _ : state) {
	srand(time(nullptr));
	int irand = rand();
	benchmark::DoNotOptimize(irand);
    }
}

// Register the function as a benchmark
BENCHMARK(BM_mt_uniform_int);
BENCHMARK(BM_mt_uniform_real);
BENCHMARK(BM_rd_mt_uniform_int);
BENCHMARK(BM_rd_mt_uniform_real);
BENCHMARK(BM_ss_mt_uniform_int);
BENCHMARK(BM_ss_mt_uniform_real);
BENCHMARK(BM_rand_int);
BENCHMARK(BM_srand_rand_int);

// Run the benchmark
BENCHMARK_MAIN();




